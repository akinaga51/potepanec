require 'web_api'
class Potepan::Api::SuggestsController < ApplicationController
  before_action :check_params, only: [:index]

  def index
    res = WebApi.get_suggests(params[:keyword], SUGGESTS_COUNT)
    if res.status == 200
      render json: JSON.parse(res.body)
    else
      render json: [], status: res.status
    end
  end

  private

  def check_params
    if params[:keyword].blank?
      render json: { error: "No Keyword" }, status: 400
    end
  end
end
