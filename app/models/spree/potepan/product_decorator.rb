module Potepan::ProductDecorator
  def related_products
    Spree::Product.joins(:taxons).where(spree_products_taxons: { taxon_id: taxons }).
      where.not(id: id).distinct
  end
  Spree::Product.prepend self
end
