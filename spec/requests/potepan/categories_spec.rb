require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET potepan/categories/id" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxon_ids: taxon.id) }

    before { get potepan_category_path(taxon.id) }

    it "リクエストが成功する事" do
      expect(response).to have_http_status(:success)
    end

    it "showテンプレートが表示する事" do
      expect(response).to render_template(:show)
    end

    it "ビューに表示される文字が適切である事" do
      expect(response.body).to include(taxon.name)
      expect(response.body).to include(taxon.products.count.to_s)
      expect(response.body).to include(product.name)
      expect(response.body).to include(product.display_price.to_s)
    end
  end
end
