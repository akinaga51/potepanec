require 'rails_helper'
require 'webmock'

RSpec.describe "Suggests", type: :request do
  describe "GET /potepan/api/suggests" do
    before do
      WebMock.enable!
    end

    context "WebApiのレスポンスが正常な場合" do
      before do
        WebMock.stub_request(:get, ENV['SUGGEST_URL']).
          with(
            headers: { 'Authorization' => "Bearer #{ENV['SUGGEST_API_KEY']}" },
            query: { 'keyword' => 'r', 'max_num' => SUGGESTS_COUNT }
          ).to_return(
            body: ["ruby", "ruby for women", "ruby for men", "rails for men", "rails for women"].to_json,
            status: 200
          )
        get potepan_api_suggests_path params: { keyword: 'r' }
      end

      it "レスポンスが正常な事" do
        expect(response.status).to eq 200
      end

      it "レスポンスの検索結果の内容が期待値通りである事" do
        expect(JSON.parse(response.body)).to eq ["ruby", "ruby for women", "ruby for men", "rails for men", "rails for women"]
      end
    end

    context "WebApiのレスポンスが異常な場合" do
      before do
        WebMock.stub_request(:get, ENV['SUGGEST_URL']).
          with(
            headers: { 'Authorization' => "Bearer #{ENV['SUGGEST_API_KEY']}" },
            query: { 'keyword' => 'r', 'max_num' => SUGGESTS_COUNT }
          ).to_return(
            body: [],
            status: 500
          )
        get potepan_api_suggests_path params: { keyword: 'r' }
      end

      it "レスポンスが異常な事" do
        expect(response.status).to eq 500
      end

      it "レスポンスの検索結果の内容が空の配列である事" do
        expect(JSON.parse(response.body)).to eq []
      end
    end

    context "keywordがnilの場合" do
      before do
        get potepan_api_suggests_path params: { keyword: nil }
      end

      it "ステータス400を返す事" do
        expect(response.status).to eq 400
      end

      it "エラーメッセージを返す事" do
        json = JSON.parse(response.body)
        expect(json['error']).to eq "No Keyword"
      end
    end

    context "keywordが空文字の場合" do
      before do
        get potepan_api_suggests_path params: { keyword: ' ' }
      end

      it "ステータス400を返す事" do
        expect(response.status).to eq 400
      end

      it "エラーメッセージを返す事" do
        json = JSON.parse(response.body)
        expect(json['error']).to eq "No Keyword"
      end
    end
  end
end
