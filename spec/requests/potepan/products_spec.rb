require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET potepan/products/id" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, name: "related", taxons: [taxon]) }

    before { get potepan_product_path(product.id) }

    it "リクエストが成功する事" do
      expect(response).to have_http_status(:success)
    end

    it "showテンプレートが表示する事" do
      expect(response).to render_template(:show)
    end

    it "ビューに表示される文字が適切である事" do
      expect(response.body).to include(product.name)
      expect(response.body).to include(product.display_price.to_s)
      expect(response.body).to include(product.description)
    end

    it "ビューに表示される関連商品の個数が適切である事" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
