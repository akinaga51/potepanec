require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe "#full_title" do
    subject { full_title(title) }

    context "''が与えられた時" do
      let(:title) { '' }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "nilが与えられた時" do
      let(:title) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "'RUBY ON RAILS TOTE'が与えられた時" do
      let(:title) { 'RUBY ON RAILS TOTE' }

      it { is_expected.to eq "RUBY ON RAILS TOTE - BIGBAG Store" }
    end
  end
end
