require 'rails_helper'

RSpec.describe "ProductDecorator", type: :model do
  let!(:taxon_1) { create(:taxon) }
  let!(:taxon_2) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon_1, taxon_2]) }
  let!(:related_products) { create_list(:product, 3, name: "related", taxons: [taxon_1, taxon_2]) }

  describe "related_products" do
    it "関連商品が含まれている事" do
      expect(product.related_products).to include(*related_products)
    end

    it "productが関連商品に含まれていない事" do
      expect(product.related_products).not_to include product
    end

    it "重複した関連商品がない事" do
      test_related_products = product.related_products
      expect(test_related_products).to eq test_related_products.uniq
    end
  end
end
