require 'rails_helper'

RSpec.describe "Products", type: :system do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "related", taxons: [taxon]) }
  let!(:other_taxon) { create(:taxon) }
  let!(:other_product) { create(:product, name: "other", taxons: [other_taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "関連商品が表示される事" do
    within '.productsContent' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price.to_s
    end
  end

  it "関連していない商品は表示されていない事" do
    within '.productsContent' do
      expect(page).not_to have_content other_product.name
    end
  end

  it "関連商品クリックで商品詳細ページが表示される事" do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end

  it "一覧ページへ戻るクリックでカテゴリーページが表示される事" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
