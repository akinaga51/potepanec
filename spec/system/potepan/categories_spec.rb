require 'rails_helper'

RSpec.describe "Categories", type: :system do
  let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy_id: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: 'RUBY ON RAILS TOTE', taxon_ids: taxon.id) }
  let(:other_taxon) { create(:taxon, name: "Mugs", taxonomy_id: taxonomy, parent_id: taxonomy.root.id) }
  let!(:other_product) { create(:product, name: 'RUBY ON RAILS MUG', taxon_ids: other_taxon.id) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "アクセスしたカテゴリーの商品情報ページを表示する事" do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price.to_s
    end
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
    end
  end

  it "他のカテゴリーの商品は表示されない事" do
    within '.productBox' do
      expect(page).not_to have_content other_product.name
    end
  end

  it "カテゴリーへのリンクを持つ事" do
    expect(page).to have_link taxon.name
    expect(page).to have_link other_taxon.name
  end

  it "個別の商品詳細ページへアクセスできる事" do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "他のカテゴリーの商品情報ページへアクセスできる事" do
    click_on other_taxon.name
    expect(current_path).to eq potepan_category_path(other_taxon.id)
  end
end
