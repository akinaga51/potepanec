class WebApi
  def self.get_suggests(keyword, max_num)
    url = ENV['SUGGEST_URL']
    header = { 'Authorization' => "Bearer #{ENV['SUGGEST_API_KEY']}" }
    query = { 'keyword' => keyword, 'max_num' => max_num }
    HTTPClient.new.get(url, query, header)
  end
end
